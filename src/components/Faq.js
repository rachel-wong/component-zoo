import React from 'react'
import Question from './Question'
import '../styles/style.css'

// another version of accordion

const Faq = () => {
  return (
    <section className="section">
      <h2>Another version of the accordion component (FAQ & Question)</h2>
      <Question title="Question goes here?" content="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin eget tortor risus. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Donec sollicitudin molestie malesuada." />
      <Question title="Question goes here?" content="Pellentesque in ipsum id orci porta dapibus. Donec sollicitudin molestie malesuada. Donec rutrum congue leo eget malesuada."/>
      <Question title="Question goes here?" content="Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Vivamus suscipit tortor eget felis porttitor volutpat. Pellentesque in ipsum id orci porta dapibus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula."/>
        <Question title="Question goes here?" content="Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Vivamus suscipit tortor eget felis porttitor volutpat. Pellentesque in ipsum id orci porta dapibus. Nulla porttitor accumsan tincidunt. Nulla quis lorem ut libero malesuada feugiat. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem.." />
    </section>
  )
}

export default Faq
