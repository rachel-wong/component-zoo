import React, { useState, useRef } from 'react'
import "../styles/faq.css"
import { FaChevronRight} from 'react-icons/fa'
// part of FAQ, another version of the accordion
const Question = (props) => {

  const [active, setActive] = useState("")
  const [height, setHeight] = useState(0)
  const [rotate, setRotate] = useState("")
  // creates reference to a DOM element, and can get data by accessing .current
  const faqContent = useRef(null)

  function toggleAccordion () {
    setActive(active === "" ? "active" : "")
    setHeight(active === "active" ? "0px" : `${faqContent.current.scrollHeight}px`)
    setRotate(active === "active" ? "" : "rotate")
    console.log("max-height of the faq content", faqContent.current.scrollHeight)
  }

  return (
    <div className="faq__section">
      <button type="button" className={`faq ${active}`} onClick={toggleAccordion}>
        <p className="faq__title">{props.title}</p>
        <FaChevronRight className={`faq__icon ${rotate}`} />
      </button>
      <div className="faq__content" ref={faqContent} style={{ maxHeight: `${height}`}}>
        <div className="faq__text" dangerouslySetInnerHTML={{ __html: props.content}}>

        </div>
      </div>
    </div>
  )
}

export default Question
