import React, { useState} from 'react'
import { AiOutlineMinus, AiOutlinePlus } from 'react-icons/ai'
import '../styles/accordion.css'

// component in Accordion.js

const SingleQuestion = ({ title, info }) => {

  const [showInfo, setShowInfo] = useState(false);

  return (
    <>
      <div className="question">
        <header>
              <h4>{title}</h4>
              <button className="btn" onClick={ () => setShowInfo(!showInfo)}>
                { showInfo == true ? (
                  <span>
                    <AiOutlineMinus />
                  </span>
                ) : (
                  <span>
                    <AiOutlinePlus />
                </span>
              )}
              </button>
        </header>
        { showInfo && <p>{info}</p> }
        </div>
      </>
  )
}

export default SingleQuestion
