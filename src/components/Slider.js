import React, { useState, useEffect } from 'react'
import '../styles/slider.css'
import { FiChevronRight, FiChevronLeft } from 'react-icons/fi'
import { FaQuoteRight } from 'react-icons/fa'
import data from '../data/dataSlider'

const Slider = () => {
  const [people, setPeople] = useState(data)
  const [index, setIndex] = useState(0)

  useEffect(() => {

    // sets the limitations on what happens when it is the first and last slides
    const lastIndex = people.length - 1
    if (index < 0) {
      setIndex(lastIndex) // index will never get into negative
    }
    if (index > lastIndex) {
      setIndex(0) // index will never go beyond the number of available slides
    }

  }, [index, people])

  // automatically move the slides
  // useEffect(() => {
  //   setInterval(() => {
  //     setIndex(index + 1)
  //   }, 3000)
  // }, [index])

  return (
    <section className="section">
      <div className="title">
        <h2><span>/</span>Slider</h2>
      </div>
      <div className="section-center">
        {people.map((person, personIndex) => {
          const { id, image, name, quote, title } = person

          // this alone is buggy when the personIndex goes into the negative
          let position = 'nextSlide'

          // switch out the css classes to scroll
          if (personIndex === index) {
            position = 'activeSlide'
          }

          // if it is the prev slide or the first slide
          if (personIndex === index - 1 || (index === 0 && personIndex === people.length - 1)) {
            position = 'lastSlide'
          }
          return (
            <article className={ position } key={personIndex} >
              <img src={image} alt={name + title} className="person-img" />
              <h4>{name}</h4>
              <p className="title">{title}</p>
              <p className="text">{quote}</p>
              <FaQuoteRight className="icon" />
            </article>
          )
        })}
        <button className="next" onClick={ () => setIndex(index - 1)}>
          <FiChevronRight />
        </button>

        <button className="prev" onClick={ () => setIndex(index + 1)}>
          <FiChevronLeft />
        </button>
      </div>
    </section>
  )
}

export default Slider