import React from 'react'
import '../styles/style.css'
import '../styles/filtermenu.css'

const FilterButtons = ({ filterItems, categories }) => {

  if (categories.length == 0) <div className="no-cats">No filter</div>

  return (
    <div className="btn-container">
      {categories.length > 0 && categories.map((category, idx) => (
        <button key={idx} className="filter-btn" onClick={() => filterItems(category)}>{ category }</button>
      ))}
    </div>
  )
}

export default FilterButtons
