import React, { useState, useEffect } from 'react'
import FilterButtons from './FilterButtons'
import '../styles/style.css'
import '../styles/filtermenu.css'
import data from '../data/dataFilter'

const FilterMenu = () => {
  const [menuItems, setMenuItems] = useState(data)
  const [categories, setCategories] = useState([])

  const filterItems = (category) => {
    if (category === "all") {
      setMenuItems(data)
      return;
    }
    const filtered = data.filter(item => item.category === category)
    setMenuItems(filtered)
  }

  useEffect(() => {
    if (menuItems) {
    const allCategories = ['all', ... new Set(menuItems.map(item => item.category))]
    setCategories(allCategories)
  }
  }, [])

  return (
    <section className="menu section">
      <h2 className="title">Filter menu</h2>
        <div className="underline"></div>
      <FilterButtons categories={categories} filterItems={ filterItems } />
        <div className="section-center menu-section-center">
          {menuItems.length > 0 && menuItems.map((item, idx) => (
            <div className="menu-item" key={ idx }>
              <img src={item.img} className="photo" />
              <div className="item-info">
                <header>
                  <h4>{item.title}</h4>
                  <h4 className="price">{ item.price }</h4>
                </header>
                <p className="item-category">{ item.category.toUpperCase() }</p>
                <p className="item-text">
                  { item.desc }
                </p>
              </div>
            </div>
          ))
          }
        </div>
    </section>
  )
}

export default FilterMenu
