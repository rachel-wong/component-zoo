import React, { useState}from 'react'
import "../styles/accordion.css"
import data from '../data/dataAccordion'
import SingleQuestion from '../components/SingleQuestion'

const Accordion = () => {

  const [questions, setQuestions] = useState(data)
  return (
    <main className="accordion">
      <section className="container">
        <h3>Accordion component (Accordion & SingleQuestion)</h3>
        <section className="info">
        {questions.map((question, idx) => (
          <SingleQuestion key={idx} {...question} />
        ))}
        </section>
      </section>
    </main>
  )
}

export default Accordion
