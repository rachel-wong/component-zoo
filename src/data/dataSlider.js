const data = [
  {
    id: 1,
    image: 'https://picsum.photos/id/1074/300/300',
    name: 'Maria Ferguson',
    title: 'office manager',
    quote: 'Donec sollicitudin molestie malesuada. Proin eget tortor risus. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Donec rutrum congue leo eget malesuada. Sed porttitor lectus nibh.'
  },
  {
    id: 2,
    image: 'https://picsum.photos/id/1005/300/300',
    name: 'Peter Ferguson',
    title: 'manager',
    quote: 'Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi..'
  },
  {
    id: 3,
    image: 'https://picsum.photos/id/1003/300/300',
    name: 'Bob Ferguson',
    title: 'officer',
    quote: 'Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Pellentesque in ipsum id orci porta dapibus. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus.'
  },
  {
    id: 4,
    image: 'https://picsum.photos/id/1001/300/300',
    name: 'Tom Ferguson',
    title: 'liaison',
    quote: 'Quisque velit nisi, pretium ut lacinia in, elementum id enim. Proin eget tortor risus. Pellentesque in ipsum id orci porta dapibus. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui.'
  },
  {
    id: 5,
    image: 'https://picsum.photos/id/1080/300/300',
    name: 'Janet Ferguson',
    title: 'something else',
    quote: 'Quisque velit nisi, pretium ut lacinia in, elementum id enim. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Donec rutrum congue leo eget malesuada. Vivamus suscipit tortor eget felis porttitor volutpat.'
  },
  {
    id: 6,
    image: 'https://picsum.photos/id/1025/300/300',
    name: 'Matilda Ferguson',
    title: 'last one',
    quote: 'Proin eget tortor risus. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Donec sollicitudin molestie malesuada..'
  },
]

export default data