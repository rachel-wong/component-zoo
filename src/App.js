import { Slider, Accordion, Faq, FilterMenu } from './components'
import './styles/style.css'

function App() {
  return (
    <div>
      <FilterMenu />
      <Accordion />
      <Slider />
      <Faq />
    </div>
  );
}

export default App;
