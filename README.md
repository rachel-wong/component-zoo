### Component Zoo - UI Components in React

* Slider :white_check_mark:
* Accordion (two versions - Accordion & Faq) :white_check_mark:
* Pagination
* Modal
* Sidebar
* Dark mode toggling
* Quiz
* Tabs
* Sort/Filter


### :eightball: Learning notes

#### Accordion v2 - Faq (Faq.js and Question.js)

- this version improves upon `Accordion.js` in that it has slide
- `useRef` hook to get the current max height of the content
- `ref` is an attribute to be applied to the require DOM element in order to access the DOM element's `current` data
- css does not specify any height or max-height, only specifies a transition for it
- a maximum height is calculated only when the accordion is in `active` state, and it is applied as an inline style
- the `active` useState is the anchoring state where rotate and height are dependent upon
- [https://medium.com/skillthrive/build-a-react-accordion-component-from-scratch-using-react-hooks-a71d3d91324b](https://medium.com/skillthrive/build-a-react-accordion-component-from-scratch-using-react-hooks-a71d3d91324b)


